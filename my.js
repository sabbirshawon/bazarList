var app = new Vue({
      el: '#app',
      computed: {
        total() {
          var total = 0
          this.items.forEach(item => {
            total = total + parseFloat(item.price)
          })
          return total
        }
      },
      data: {
        items: [
          { name: 'demo', price: 10.0 }
        ],
        newItem: ''
      },
      methods: {
        addItem() {
          this.items.push({
            name: this.newItem,
            price: 0.00
          })
          this.newItem = ''
        },
        removeItem(index) {
          this.items.splice(index, 1)
        }
      }
    })
